<?php

namespace JontyNewman;

use GuzzleHttp\Psr7\Response as PsrResponse;
use GuzzleHttp\Psr7\ServerRequest;
use JontyNewman\Oku\Aggregate;
use JontyNewman\Oku\Data\Repository as DataRepository;
use JontyNewman\Oku\IO\Repository as IoRepository;
use JontyNewman\Oku\RequestHandler as Oku;
use JontyNewman\Oku\Upload\Repository as UploadRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use ShrooPHP\Core\Request;
use ShrooPHP\Core\Session;
use ShrooPHP\Framework\Application as App;
use ShrooPHP\Framework\Request\Responses\Response;
use ShrooPHP\RESTful\Collection;
use ShrooPHP\RESTful\Collection\Traits\HashTable;
use ShrooPHP\RESTful\Collection\Traits\Unimplemented;
use ShrooPHP\RESTful\Request\Handler;
use ShrooPHP\RESTful\Resource;
use ShrooPHP\RESTful\Resource\Traits\File as FileTrait;
use ShrooPHP\PSR\RequestHandler;

/**
 * A simple content management system.
 */
class CMS implements RequestHandlerInterface
{
    /**
     * The name of the IO repository within the aggregate.
     */
    const INPUT_IO = 'io';

    /**
     * The name of the upload repository within the aggregate.
     */
    const INPUT_UPLOAD = 'upload';

    /**
     * The name of the data repository within the aggregate.
     */
    const INPUT_DATA = 'data';

    /**
     * The default request path reserved for the 404 page.
     */
    const DEFAULT = '/404';

    /**
     * The repository being used to persist input files.
     *
     * @var \JontyNewman\Oku\IO\Repository
     */
    private $sources;

    /**
     * The repository being used to persist uploads.
     *
     * @var \JontyNewman\Oku\Upload\Repository
     */
    private $uploads;

    /**
     * The editor being used to edit content (or NULL if edit mode is not
     * enabled).
     *
     * @var callable
     */
    private $editor;

    /**
     * The request path being reserved for the 404 page.
     *
     * @var string
     */
    private $default;

    /**
     * The expected origin (if any).
     *
     * @var string
     */
    private $origin;

    /**
     * The key being used for method overriding (or NULL if the default is being
     * used).
     *
     * @var string
     */
    private $method;

    /**
     * The key being used for token specification (or NULL if the default is
     * being used).
     *
     * @var string
     */
    private $token;

    /**
     * The key being used to flag inset rendering when in edit mode (or NULL if
     * the default is being used).
     *
     * @var string
     */
    private $inset;

    /**
     * The request handler being used to handle requests.
     *
     * @var \ShrooPHP\PSR\RequestHandler
     */
    private $handler;

    /**
     * The repository being used for persistence.
     *
     * @var \JontyNewman\Oku\Aggregate
     */
    private $repository;

    /**
     * The default response.
     *
     * @var \GuzzleHttp\Psr7\Response
     */
    private $response;

    /**
     * The handler being used to handle asset requests.
     *
     * @var \ShrooPHP\RESTful\Request\Handler
     */
    private $assets;

    /**
     * The collection containing assets.
     *
     * @var \ShrooPHP\RESTful\Collection
     */
    private $collection;

    /**
     * Constructs a simple content management system.
     *
     * @param \JontyNewman\Oku\IO\Repository $sources The repository to use for
     * input files.
     * @param \JontyNewman\Oku\Upload\Repository $uploads The repository to use
     * for uploads.
     * @param callable $editor The editor to use for editing content (or NULL to
     * disable edit mode).
     * @param string $default The request path reserved for the 404 page (or
     * NULL to use the default).
     * @param string $origin The expected origin (if any).
     * @param string $method The key to use for method overriding (or NULL to
     * use the default).
     * @param string $token The key to use for token specification (or NULL to
     * use the default).
     * @param string $inset The key to flag inset rendering when in edit mode
     * (or NULL to use the default).
     */
    public function __construct(
            IoRepository $sources,
            UploadRepository $uploads,
            DataRepository $data,
            callable $editor = null,
            string $default = null,
            string $origin = null,
            string $method = null,
            string $token = null,
            string $inset = null
    ) {

        $this->sources = $sources;
        $this->uploads = $uploads;
        $this->data = $data;
        $this->editor = $editor;
        $this->default = $default ?? self::DEFAULT;
        $this->origin = $origin;
        $this->method = $method;
        $this->token = $token;
        $this->inset = $inset;
    }

    /**
     * Gets the aggregate currently associated with the content management system.
     *
     * @return \JontyNewman\Oku\Aggregate
     */
    public function repository(): Aggregate
    {
        if (is_null($this->repository)) {
            $this->repository = $this->toRepository();
        }

        return $this->repository;
    }

    /**
     * Adds an immutable asset to the content management system.
     *
     * @param string $path The request path of the asset.
     * @param string $filename The filename of the asset.
     * @param string $type The content type of the asset.
     */
    public function asset(string $path, string $filename, string $type): void
    {
        $this->collection()->add($path, $this->toAsset($filename, $type));
    }

    /**
     * {@inheritDoc}
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->handler()->handle($request);
    }

    /**
     * Extracts the current request from the server API and renders a response
     * accordingly.
     */
    public function serve(): void
    {
        $this->handler()->serve();
    }

    /**
     * Gets the request handler currently associated with the content management
     * system.
     *
     * @return \ShrooPHP\PSR\RequestHandler The request handler currently
     * associated with the content management system.
     */
    private function handler(): RequestHandler
    {
        if (is_null($this->handler)) {
            $this->handler = $this->toRequestHandler();
        }

        return $this->handler;
    }

    /**
     * Generates an aggregate repository for the content management system.
     *
     * @return \JontyNewman\Oku\Aggregate The generated aggregate repository.
     */
    private function toRepository(): Aggregate
    {
        $repository = new Aggregate();
        $repository->set(self::INPUT_IO, $this->sources);
        $repository->set(self::INPUT_UPLOAD, $this->uploads);
        $repository->set(self::INPUT_DATA, $this->data);

        return $repository;
    }

    /**
     * Generates a request handler for the content management system.
     *
     * @return \ShrooPHP\PSR\RequestHandler The generated request handler.
     */
    private function toRequestHandler(): RequestHandler
    {
        $handler = new RequestHandler($this->response());

        $handler->apply($this->toApplicationModifier());

        return $handler;
    }

    /**
     * Gets the default response currently associated with the content
     * management system.
     *
     * @return \GuzzleHttp\Psr7\Response The default response currently
     * associated with the content management system.
     */
    private function response(): PsrResponse
    {
        if (is_null($this->response)) {
            $this->response = $this->toResponse();
        }

        return $this->response;
    }

    /**
     * Generates the default response.
     *
     * @return \GuzzleHttp\Psr7\Response The generated response.
     */
    private function toResponse(): PsrResponse
    {
        return new PsrResponse(Response::HTTP_NOT_FOUND);
    }

    /**
     * Generates an application modifier for the content management system.
     *
     * @return callable The generated application modifier.
     */
    private function toApplicationModifier(): callable
    {
        return function (App $app): void {

            $app->push(function (Request $request): ?Response {

                $code = Response::HTTP_METHOD_NOT_ALLOWED;
                $response = $this->assets()->respond($request);

                if (!is_null($response) && $code === $response->code()) {
                    $response = null;
                }

                return $response;
            });

            $app->path($this->default, function (App $app): void {

                $app->method('GET', function (App $app): void {

                    $app->push($this->toDefaultCallback());
                });
            });

            $app->push(function (Request $request): Response {

                $oku = $this->toOku($request->session());
                $response = $oku->__invoke($request);

                if (is_null($response)) {
                    $response = ($this->toDefaultCallback($oku))($request);
                }

                return $response;
            });
        };
    }

    /**
     * Generates a request handler for the content management system.
     *
     * @param \ShrooPHP\Core\Session $session The session to associate with the
     * request handler.
     * @return \JontyNewman\Oku\RequestHandler The generated request handler.
     */
    private function toOku(Session $session): Oku
    {
        return new Oku(
                $this->repository(),
                $this->response(),
                $this->editor,
                $this->origin,
                $session,
                $this->method,
                $this->token,
                $this->inset
        );
    }

    /**
     * Converts the given request handler to the default callback.
     *
     * @param \JontyNewman\Oku\RequestHandler $oku The request handler to
     * convert.
     * @return callable The converted request handler.
     */
    private function toDefaultCallback(Oku $oku = null): callable
    {
        return function (Request $request) use ($oku): Response {

            if (is_null($oku)) {
                $oku = $this->toOku($request->session());
            }

            $default = new ServerRequest('GET', $this->default);
            $params = $request->params()->getArrayCopy();
            $response = $oku->handle($default->withQueryParams($params));

            return $this->toDefaultResponse($response);
        };
    }

    /**
     * Converts the given PSR-compliant response to a compatible default
     * response.
     *
     * @param \Psr\Http\Message\ResponseInterface $response The response to
     * convert.
     * @return \ShrooPHP\Core\Request\Response The converted response.
     */
    private function toDefaultResponse(ResponseInterface $response): Response
    {
        $converted = Response::callback(function () use ($response) {

            $detached = $response->getBody()->detach();

            if (!is_null($detached)) {
                $this->passthru($detached);
            }
        });

        $converted->setCode(Response::HTTP_NOT_FOUND);

        foreach ($response->getHeaders() as $header => $values) {
            foreach ($values as $value) {
                $converted->addHeader($header, $value);
            }
        }

        return $converted;
    }

    /**
     * Passes through the given resource.
     *
     * @param resource $handle The resource to pass through.
     * @throws \RuntimeException Unable to pass through the resource.
     */
    private function passthru($handle): void
    {
        if (false === fpassthru($handle)) {
            throw new RuntimeException('Unable to pass through body');
        }

        if (!fclose($handle)) {
            throw new RuntimeException('Unable to close body');
        }
    }

    /**
     * Gets the request handler currently associated with the assets of the
     * content management system.
     *
     * @return \ShrooPHP\RESTful\Request\Handler The request handler currently
     * associated with the assets of the content management system.
     */
    private function assets(): Handler
    {

        if (is_null($this->assets)) {
            $this->assets = $this->toAssets();
        }

        return $this->assets;
    }

    /**
     * Generates a request handler for the assets of the content management
     * system.
     *
     * @return \ShrooPHP\RESTful\Request\Handler The generated request handler.
     */
    private function toAssets(): Handler
    {
        return new Handler($this->collection());
    }

    /**
     * Generates an asset.
     *
     * @param string $path The path of the file representing the asset.
     * @param string $type The content type of the asset.
     * @param int $buffer The chunk size of the asset (or NULL to use the
     * default).
     * @return \ShrooPHP\RESTful\Resource The generated asset.
     */
    private function toAsset(
            string $path,
            string $type = null,
            int $buffer = null
    ): Resource {

        return new class($path, $type, $buffer) implements Resource {

            const BUFFER = \ShrooPHP\RESTful\Resources\File::BUFFER;

            use FileTrait;

            private $path;

            private $type;

            private $buffer;

            public function __construct(
                    string $path,
                    string $type = null,
                    int $buffer = null
            ) {
                $this->path = $path;
                $this->type = $type;
                $this->buffer = $buffer ?? self::BUFFER;
            }

            public function type(): ?string
            {
                return $this->type;
            }

            protected function path(): string
            {
                return $this->path;
            }

            protected function buffer(): int
            {
                return $this->buffer;
            }
        };
    }

    /**
     * Gets the collection currently associated with the assets of the content
     * management system.
     *
     * @return \ShrooPHP\RESTful\Collection The collection currently associated
     * with the assets of the content management system.
     */
    private function collection(): Collection
    {
        if (is_null($this->collection)) {
            $this->collection = $this->toCollection();
        }

        return $this->collection;
    }

    /**
     * Generates the collection of assets for the content management system.
     *
     * @return \ShrooPHP\RESTful\Collection The generated collection.
     */
    private function toCollection(): Collection
    {
        return new class implements Collection {

            use HashTable,
                Unimplemented {
                HashTable::get insteadof Unimplemented;
                Unimplemented::post insteadof HashTable;
                HashTable::put as add;
                Unimplemented::put insteadof HashTable;
                Unimplemented::patch insteadof HashTable;
                Unimplemented::delete insteadof HashTable;
            }

            protected function limit(): ?int
            {
                return null;
            }
        };
    }
}
